import { EngineersProvider } from "./hocs/context/engineersContext";
import { SelectedDateProvider } from "./hocs/context/selectedDateContext";
import { ShiftsProvider } from "./hocs/context/shiftsContext";
import AppLayout from "./navigation/AppLayout";

const App = () => {
  let content: JSX.Element;
  content = <AppLayout />;

  return (
    <>
      <EngineersProvider>
        <SelectedDateProvider>
          <ShiftsProvider>{content}</ShiftsProvider>
        </SelectedDateProvider>
      </EngineersProvider>
    </>
  );
};

export default App;
