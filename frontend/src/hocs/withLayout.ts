import { connect } from "react-redux";
import { ComponentType } from "react";
import { AppState } from "../store";
import { updatePageTitle } from "../store/layout/actions";
import { LayoutState } from "../store/layout/types";

export interface LayoutActions {
  setPageTitle: typeof updatePageTitle;
}

export type LayoutProps = LayoutState & LayoutActions;

function withLayout<T extends LayoutProps>(element: ComponentType<T>) {
  const mapStateToProps = (state: AppState, ownProps: {}): LayoutState =>
    ({
      pageTitle: state.layout.pageTitle,
    } as LayoutState);

  return connect(mapStateToProps, {
    setPageTitle: updatePageTitle,
  } as LayoutActions)(element as any);
}

export default withLayout;
