import { createContext, useContext, useEffect, useMemo, useState } from "react";
import client from "../../api/client";
import { Engineer } from "../../api/engineer/objects/Engineer";

function useEngineersValue() {
  const [engineers, setEngineers] = useState<Engineer[]>([]);
  const [isEngineersRequested, setIsEngineersRequested] = useState(false);

  const value = useMemo(
    () => ({
      engineers,
    }),
    [engineers]
  );

  useEffect(() => {
    const getEngineers = async () => {
      setEngineers(await client.engineer.getEngineers());
    };

    if (engineers.length === 0 && !isEngineersRequested) {
      getEngineers();
      setIsEngineersRequested(true);
    }
  }, [engineers, isEngineersRequested]);

  return value;
}

export type EngineersContextType = ReturnType<typeof useEngineersValue>;

const EngineersContext = createContext<EngineersContextType | undefined>(undefined);
EngineersContext.displayName = "EngineersContext";

export const EngineersProvider: React.FC = (props) => {
  const engineers = useEngineersValue();
  return <EngineersContext.Provider value={engineers} {...props} />;
};

export function useEngineersContext() {
  const context = useContext(EngineersContext);
  if (context === undefined) {
    throw new Error("useEngineersContext must be used within a Provider");
  }
  return context;
}
