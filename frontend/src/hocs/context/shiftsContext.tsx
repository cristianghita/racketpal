import { createContext, useContext, useEffect, useMemo, useState } from "react";
import client from "../../api/client";
import { Shift } from "../../api/shift/objects/Shift";
import { useSelectedDateContext } from "./selectedDateContext";

function useShiftsValue() {
  const { selectedDate } = useSelectedDateContext();
  const [shifts, setShifts] = useState<Shift[]>([]);

  const refreshShifts = async () => {
    setShifts(await client.shift.getShifts(selectedDate!));
  };

  const value = useMemo(
    () => ({
      shifts,
      refreshShifts,
    }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [shifts]
  );

  useEffect(() => {
    const getShifts = async () => {
      setShifts(await client.shift.getShifts(selectedDate!));
    };

    getShifts();
  }, [selectedDate]);

  return value;
}

export type ShiftsContextType = ReturnType<typeof useShiftsValue>;

const ShiftsContext = createContext<ShiftsContextType | undefined>(undefined);
ShiftsContext.displayName = "ShiftsContext";

export const ShiftsProvider: React.FC = (props) => {
  const shifts = useShiftsValue();
  return <ShiftsContext.Provider value={shifts} {...props} />;
};

export function useShiftsContext() {
  const context = useContext(ShiftsContext);
  if (context === undefined) {
    throw new Error("useShiftsContext must be used within a Provider");
  }
  return context;
}
