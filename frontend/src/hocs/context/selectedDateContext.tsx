import { createContext, useContext, useEffect, useMemo, useState } from "react";
import { DateToUnix, GetCurrentDateWithoutTime } from "../../util/dateUtil";

function useSelectedDateValue() {
  const [selectedDate, setSelectedDate] = useState<number>();

  const value = useMemo(
    () => ({
      selectedDate,
      setSelectedDate,
    }),
    [selectedDate]
  );

  useEffect(() => {
    if (!selectedDate) {
      setSelectedDate(DateToUnix(GetCurrentDateWithoutTime()));
    }
  }, [selectedDate]);

  return value;
}

export type SelectedDateContextType = ReturnType<typeof useSelectedDateValue>;

const SelectedDateContext = createContext<SelectedDateContextType | undefined>(undefined);
SelectedDateContext.displayName = "SelectedDateContext";

export const SelectedDateProvider: React.FC = (props) => {
  const selectedDate = useSelectedDateValue();
  return <SelectedDateContext.Provider value={selectedDate} {...props} />;
};

export function useSelectedDateContext() {
  const context = useContext(SelectedDateContext);
  if (context === undefined) {
    throw new Error("useSelectedDateContext must be used within a Provider");
  }
  return context;
}
