export class CustomError extends Error {
  statusCode?: number;
  message: string = "";
  error?: string;
  errorCode?: number;

  constructor({ message, statusCode }: CustomError) {
    super();
    this.message = message;
    this.statusCode = statusCode;
  }
}
