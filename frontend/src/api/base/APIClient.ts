import axios from "axios";
import { CustomError } from "./CustomError";

export interface ClientOptions {
  apiRootURL: string;
}

interface HeadersType {
  [key: string]: string;
}

export interface FetchParams extends RequestInit {
  bodyObject?: object | null;
}

export class APIClient {
  private _apiRootURL: string = "";

  get apiRootURL(): string {
    return this._apiRootURL;
  }

  constructor({ apiRootURL }: ClientOptions) {
    this._apiRootURL = apiRootURL.trim();
    if (this._apiRootURL.endsWith("/")) {
      this._apiRootURL = this._apiRootURL.substr(0, this._apiRootURL.length - 1);
    }
  }

  _getDefaultHeaders() {
    const headers: HeadersType = {
      "Content-Type": "application/json",
    };

    return headers;
  }

  async fetch(
    strURIPath: string,
    { headers = {}, bodyObject = null, ...rest }: FetchParams,
    isThirdParty: boolean = false
  ) {
    let body;
    if (bodyObject) {
      body = JSON.stringify(bodyObject);
    }

    const objOptions = {
      headers: Object.assign({}, this._getDefaultHeaders(), headers),
      body: body ?? rest.body,
      ...rest,
    };

    if (!strURIPath.startsWith("/") && !isThirdParty) {
      strURIPath = `/${strURIPath}`;
    }

    const strRequestURL = encodeURI(`${isThirdParty ? "" : this._apiRootURL}${strURIPath}`);

    if (isThirdParty && "GET" === objOptions.method) {
      const thirdPartyResponse = await axios.get(strRequestURL);
      return thirdPartyResponse.data;
    }

    const response = await fetch(strRequestURL, objOptions as any);
    if (response.status >= 200 && response.status < 400) {
      const parsedResponse = await response.json();
      return parsedResponse;
    }

    const customError = new CustomError({
      message: `Custom Error: Status ${response.status} - "${response.statusText}"`,
      statusCode: response.status,
      name: "Custom Error",
    });

    throw customError;
  }

  async post(strURIPath: string, options: FetchParams = {}) {
    return this.fetch(strURIPath, { method: "POST", ...options });
  }

  async get(strURIPath: string, options: FetchParams = {}, isThirdParty: boolean = false) {
    return this.fetch(strURIPath, { method: "GET", ...options }, isThirdParty);
  }

  async put(strURIPath: string, options: FetchParams = {}) {
    return this.fetch(strURIPath, { method: "PUT", ...options });
  }

  async delete(strURIPath: string, options: FetchParams = {}) {
    return this.fetch(strURIPath, { method: "DELETE", ...options });
  }
}
