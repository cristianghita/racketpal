import { getRootAPI } from "../util/env";
import { APIClient, ClientOptions } from "./base/APIClient";

export type APIClientClassType<T extends APIClient, O extends ClientOptions> = new (
  options: O
) => T;

class APIClientFactory {
  static make<T extends APIClient, O extends ClientOptions>(
    ServiceTypeConstructor: APIClientClassType<T, O>,
    prefix?: string
  ): T {
    const clientOptions = {
      apiRootURL: `${getRootAPI()}${prefix ?? ""}`,
    } as O;

    const apiClient: T = new ServiceTypeConstructor(clientOptions);
    return apiClient;
  }
}

export default APIClientFactory;
