import { APIClient } from "../base/APIClient";
import { Shift } from "./objects/Shift";

export class ShiftApiClient extends APIClient {
  //#region CREATE
  //#endregion

  //#region READ

  async getShifts(date: number): Promise<Shift[]> {
    const shifts: Shift[] = (await this.get(`/get-shifts/${date}`)).response.shifts;
    return shifts;
  }

  //#endregion

  //#region UPDATE
  //#endregion

  //#region DELETE

  async deleteShifts(): Promise<void> {
    await this.get(`/delete-shifts`);
  }

  //#endregion
}
