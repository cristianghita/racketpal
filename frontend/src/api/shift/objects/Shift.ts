export interface Shift {
  date: number;
  am: string;
  pm: string;
}
