import { APIClient } from "../base/APIClient";
import { Engineer } from "./objects/Engineer";

export class EngineerApiClient extends APIClient {
  //#region CREATE

  async createEngineer(name: string): Promise<void> {
    await this.post("/create-engineer", {
      bodyObject: { name },
    });
  }

  //#endregion

  //#region READ

  async getEngineers(): Promise<Engineer[]> {
    const engineers: Engineer[] = (await this.get(`/get-engineers`)).response.engineers;
    return engineers;
  }

  async getWinners(date: number): Promise<Engineer[]> {
    const res = await this.get(`/get-winners/${date}`);
    if (res.response.isError) {
      throw new Error(res.e.message ?? "isError: true");
    } else {
      const winners: Engineer[] = res.response.winners;
      return winners;
    }
  }

  //#endregion

  //#region UPDATE
  //#endregion

  //#region DELETE
  //#endregion
}
