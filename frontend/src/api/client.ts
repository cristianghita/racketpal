import APIClientFactory from "./APIClientFactory";
import { EngineerApiClient } from "./engineer/EngineerApiClient";
import { ShiftApiClient } from "./shift/ShiftApiClient";

const client = {
  engineer: APIClientFactory.make(EngineerApiClient, "/engineer"),
  shift: APIClientFactory.make(ShiftApiClient, "/shift"),
};

export default client;
