import { createMuiTheme } from "@material-ui/core";
import defaultPaletteColor from "./colors";

export const themeConfig = {
  palette: {
    primary: defaultPaletteColor,
    background: {
      default: "#FBFCFE",
      paper: "#FFFFFF",
    },
  },
};

export default createMuiTheme(themeConfig);
