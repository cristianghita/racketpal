import { createStyles, fade, makeStyles, Theme } from "@material-ui/core";
import { blue, orange } from "./colors";

const classicBlue = (theme: Theme) => {
  return {
    display: "flex",
    background: blue.highlight.highlight0,
    transition: theme.transitions.create(["background", "background-color"], {
      duration: theme.transitions.duration.shorter,
    }),
    color: blue.natural.natural2,
    cursor: "pointer",
    "&:hover": {
      background: blue.highlight.highlight1,
      [theme.breakpoints.down("xs")]: {
        background: blue.highlight.highlight0,
      },
    },
    "&:disabled": {
      background: fade(blue.shades.shade2, 0.7),
      color: blue.natural.natural3,
    },
    // fontFamily: "HalisGR-book",
  };
};

const classicOrange = (theme: Theme) => {
  return {
    display: "flex",
    background: orange.highlight.highlight0,
    transition: theme.transitions.create(["background", "background-color"], {
      duration: theme.transitions.duration.shorter,
    }),
    color: orange.discreet.discreet2,
    cursor: "pointer",
    "&:hover": {
      background: orange.shades.shade1,
      [theme.breakpoints.down("xs")]: {
        background: orange.highlight.highlight0,
      },
    },
    "&:disabled": {
      background: fade(orange.skipShade.skipShade1, 0.7),
      color: orange.discreet.discreet2,
    },
  };
};

const useButtonStyles = makeStyles((theme: Theme) =>
  createStyles({
    classicBlue: classicBlue(theme),
    classicOrange: classicOrange(theme),
  })
);

export default useButtonStyles;
