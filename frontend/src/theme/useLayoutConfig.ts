export interface LayoutConfig {
  content: {
    shouldShift: boolean;
  };
  navbar: {
    shouldShift: boolean;
    showMenuButton: boolean;
  };
}

function useLayoutConfig() {
  return {
    content: {
      shouldShift: false,
    },
    navbar: {
      shouldShift: false,
      showMenuButton: true,
    },
  };
}

export default useLayoutConfig;
