// * logo blue color palette https://mycolor.space/?hex=%23296BC7&sub=1
// * logo orange color palette https://mycolor.space/?hex=%23F15B24&sub=1

const defaultPaletteColor = {
  main: "#296bc7", // "#cce6ff", //"#c7c7c7", //"#808080",
  contrastText: "#FFFFFF",
};

export const logoBlue = "#296BC7";
export const logoOrange = "#F15B24";

export const blue = {
  shades: {
    shade0: "#296BC7",
    shade1: "#5286E6",
    shade2: "#74A2FF",
    shade3: "#95BFFF",
    shade4: "#B5DDFF",
  },
  randomShades: {
    shade0: "#296BC7",
    shade1: "#9CC6FF",
    shade2: "#004CA3",
    shade3: "#00175F",
    shade4: "#C3EBFF",
  },
  collective: {
    collective0: "#296BC7",
    collective1: "#7756D0",
    collective2: "#CE2556",
  },
  dust: {
    dust0: "#296BC7",
    dust1: "#424656",
    dust2: "#A7AABD",
    dust3: "#FC748C",
  },
  discreet: {
    discreet0: "#296BC7",
    discreet1: "#828FBB",
    discreet2: "#F7F9FF",
    discreet3: "#EEE8A9",
  },
  highlight: {
    highlight0: "#296BC7",
    highlight1: "#5DA9FF",
    highlight2: "#E6F4F1",
    highlight3: "#EEE8A9",
  },
  greyFriends: {
    greyFriend0: "#296BC7",
    greyFriend1: "#424656",
    greyFriend2: "#A7AABD",
  },
  natural: {
    natural0: "#296BC7",
    natural1: "#828FBB",
    natural2: "#F6F9FF",
    natural3: "#E6F4F1",
  },
  matchingGradient: {
    matchingGradient0: "#296BC7",
    matchingGradient1: "#6368C1",
    matchingGradient2: "#8465B8",
    matchingGradient3: "#9B64AD",
    matchingGradient4: "#AC65A2",
    matchingGradient5: "#B86797",
  },
  genericGradient: {
    genericGradient0: "#296BC7",
    genericGradient1: "#0095E1",
    genericGradient2: "#00B8D6",
    genericGradient3: "#00D6B0",
    genericGradient4: "#89EC85",
    genericGradient5: "#F9F871",
  },
};

export const orange = {
  dust: {
    dust0: "#F15B24",
    dust1: "#55433C",
    dust2: "#BDA69E",
    dust3: "#1DA219",
  },
  discreet: {
    discreet0: "#F15B24",
    discreet1: "#B88572",
    discreet2: "#FFF6F1",
    discreet3: "#3A001E",
  },
  highlight: {
    highlight0: "#F15B24",
    highlight1: "#DC9C85",
    highlight2: "#EDE9D0",
    highlight3: "#3A001E",
  },
  skipShade: {
    skipShade0: "#F15B24",
    skipShade1: "#FF7760",
    skipShade2: "#FF9997",
    skipShade3: "#FFBDCB",
  },
  greyFriends: {
    greyFriend0: "#F15B24",
    greyFriend1: "#55433C",
    greyFriend2: "#BDA69E",
  },
  shades: {
    shade0: "#F15B24",
    shade1: "#CB3A00",
    shade2: "#A61200",
    shade3: "#830000",
    shade4: "#630000",
  },
  randomShades: {
    randomShade0: "#F15B24",
    randomShade1: "#FFAE71",
    randomShade2: "#BD2D00",
    randomShade3: "#5A0000",
    randomShade4: "#FFB779",
  },
};

export default defaultPaletteColor;
