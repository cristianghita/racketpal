import queryString from "query-string";
import { getEnvVariable } from "./env";

export interface URLParams {
  [key: string]: string | string[] | number | number[] | boolean | boolean[];
}

export const HOMEPAGE_URL = "/";

export const getBasePath = (): string => {
  let basePath = getEnvVariable("REACT_APP_BASE_PATH").trim();

  if (basePath === "/" || basePath === "") {
    return "";
  }

  if (!basePath.startsWith("/")) {
    basePath = "/" + basePath;
  }

  if (basePath.endsWith("/")) {
    basePath = basePath.substr(0, basePath.length - 1);
  }

  return basePath;
};

export const getDefaultURI = (urlParams?: URLParams) => getHomepageURI(urlParams);

export const getURLWithParams = (baseURL: string, urlParams?: URLParams): string => {
  if (baseURL.trim().length > 1 && baseURL.endsWith("/")) {
    baseURL = baseURL.substr(0, baseURL.length - 1);
  }

  return `${baseURL}${urlParams ? `?${queryString.stringify(urlParams)}` : ""}`;
};

export const getHomepageURI = (urlParams?: URLParams): string =>
  getURLWithParams(`${getBasePath()}${HOMEPAGE_URL}`, urlParams);
