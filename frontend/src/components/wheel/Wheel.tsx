/* eslint-disable react-hooks/exhaustive-deps */
import { Button, createStyles, Grid, makeStyles, Theme } from "@material-ui/core";
import { PlayCircleFilled } from "@material-ui/icons";
import { useSpring } from "@react-spring/core";
import clsx from "clsx";
import { Dispatch, Fragment, SetStateAction, useEffect, useState } from "react";
import { animated } from "react-spring";
import client from "../../api/client";
import { Engineer } from "../../api/engineer/objects/Engineer";
import { useEngineersContext } from "../../hocs/context/engineersContext";
import { useSelectedDateContext } from "../../hocs/context/selectedDateContext";
import { useShiftsContext } from "../../hocs/context/shiftsContext";
import { blue } from "../../theme/colors";
import useButtonStyles from "../../theme/useButtonStyles";
import { DateToUnix, GetCurrentDateWithoutTime, OffsetDaysUnix } from "../../util/dateUtil";
import { WheelConfig } from "./WheelConfig";
import { WheelItem } from "./WheelItem";

const itemsCount = 10;
const OFFSET = 0.75 - 1 / itemsCount / 2; // start wheel with selected item being the first of the array

export interface WheelProps {
  setAm: Dispatch<SetStateAction<Engineer | undefined>>;
  setPm: Dispatch<SetStateAction<Engineer | undefined>>;
  setIsLoading: Dispatch<SetStateAction<boolean>>;
  isLoading: boolean;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      width: "150px",
    },
    svg: {
      width: "600px",
      height: "600px",
    },
  })
);

export const Wheel = (incomingProps: WheelProps) => {
  const buttons = useButtonStyles();
  const classes = useStyles();
  const { refreshShifts } = useShiftsContext();

  const { setAm, setPm, isLoading, setIsLoading } = incomingProps;
  const { engineers } = useEngineersContext();
  const { selectedDate } = useSelectedDateContext();
  const { shifts } = useShiftsContext();

  const r = 200;
  const cx = 250;
  const cy = 250;

  const [power, setPower] = useState(0);
  const config: WheelConfig = { mass: 50, tension: 200, friction: 200, precision: 0.001 };
  const [props, set] = useSpring(() => ({ transform: "rotate(0deg)", immediate: false }));
  const [wheelItems, setWheelItems] = useState<WheelItem[]>([]);

  const map = (value: number, in_min: number, in_max: number, out_min: number, out_max: number) => {
    if (value === 0) {
      return out_min;
    }
    return ((value - in_min) * (out_max - out_min)) / (in_max - in_min) + out_min;
  };

  const isPreviousDayFilled = (): boolean => {
    return !!shifts.find((s) => s.date === OffsetDaysUnix(selectedDate!, -1));
  };

  const isDateInPast = (): boolean => {
    return selectedDate! < DateToUnix(GetCurrentDateWithoutTime());
  };

  const generateWheelItems = () => {
    const items: WheelItem[] = [];
    const count = engineers.length;

    for (let i = 0; i < count; i++) {
      let xLength = Math.cos(2 * Math.PI * (i / count + OFFSET)) * (r - 5);
      let yLength = Math.sin(2 * Math.PI * (i / count + OFFSET)) * (r - 5);
      let txLength = Math.cos(2 * Math.PI * ((i + 0.5) / count + OFFSET)) * (r / 2);
      let tyLength = Math.sin(2 * Math.PI * ((i + 0.5) / count + OFFSET)) * (r / 2);

      items.push({
        x1: cx + xLength,
        x2: cx,
        y1: cy + yLength,
        y2: cy,
        x: cx + txLength,
        y: cy + tyLength,
        index: i,
        name: engineers[i].name,
      });
    }

    setWheelItems(items);
  };

  const selectWinners = async () => {
    try {
      setIsLoading(true);
      const winners = await client.engineer.getWinners(selectedDate!);

      searchItem(winners[0].name);
      await new Promise((resolve) => setTimeout(resolve, 5000));
      setAm(winners[0]);

      searchItem(winners[1].name);
      await new Promise((resolve) => setTimeout(resolve, 5000));
      setPm(winners[1]);

      await refreshShifts();
    } catch (e) {
      // log error
    } finally {
      setIsLoading(false);
    }
  };

  const renderItems = () => {
    const items = [];
    for (let wheelItem of Object.values(wheelItems)) {
      items.push(
        <Fragment key={wheelItem.index}>
          <line
            stroke={blue.shades.shade0}
            strokeWidth="2"
            x1={wheelItem.x1}
            y1={wheelItem.y1}
            x2={wheelItem.x2}
            y2={wheelItem.y2}
          />
          <text
            x={wheelItem.x}
            y={wheelItem.y}
            fontSize="15px"
            color={blue.greyFriends.greyFriend1}
            transform={`rotate(${((wheelItem.index + 0.5) / itemsCount + OFFSET) * 360}
                  ${wheelItem.x},
                  ${wheelItem.y})`}
          >
            {wheelItem.name}
          </text>
        </Fragment>
      );
    }

    return items;
  };

  useEffect(() => {
    if (wheelItems.length === 0 && engineers.length > 0) {
      generateWheelItems();
    }
    set({
      from: { transform: `rotate(${map(power, 0, 100, 0, 700)}deg)` },
      transform: `rotate(-${map(power, 0, 100, 0, 700)}deg)`,
      immediate: false,
      config,
    });
  }, [power, engineers]);

  const searchItem = (item: string) => {
    const wheelItem = wheelItems.find((i) => i.name === item)!;
    const itemsCount = wheelItems.length;
    const startItem = wheelItems.reduce((prev, current) => (prev.y < current.y ? prev : current));
    const itemsToRotate = itemsCount - (startItem.index + 1) + ((wheelItem.index + 1) % itemsCount);
    const powerToRotate = (62 / 12) * itemsToRotate;
    setPower(104 + powerToRotate); // 52 = 1 full rotations
  };

  return (
    <Grid container direction="column" spacing={1} alignItems="center" justify="center">
      <Grid item xs={12}>
        {engineers.length > 0 ? (
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox={`0 0 ${cx * 2} ${cy * 2}`}
            className={classes.svg}
          >
            <g fill={blue.shades.shade4} stroke={blue.shades.shade0} strokeWidth="10">
              <circle cx={cx} cy={cy} r={r} />
            </g>
            <animated.g
              style={{
                transform: props.transform,
                transformOrigin: "center",
              }}
            >
              {renderItems()}
            </animated.g>
            <g fill={blue.dust.dust2}>
              <circle cx={cx} cy={cy} r={r / 13} />
            </g>
            <g fill={blue.dust.dust1}>
              <circle cx={cx} cy={cy} r={r / 30} />
            </g>
            <g fill={blue.dust.dust2} stroke={blue.dust.dust1} strokeWidth="2">
              <polygon points="250,70 230,30 270,30" />
            </g>
          </svg>
        ) : null}
      </Grid>
      <Grid item xs={12}>
        <Button
          className={clsx(buttons.classicOrange, classes.button)}
          fullWidth
          onClick={() => selectWinners()}
          startIcon={<PlayCircleFilled />}
          disabled={
            isLoading ||
            engineers.length === 0 ||
            (!isPreviousDayFilled() && selectedDate !== DateToUnix(GetCurrentDateWithoutTime())) ||
            isDateInPast()
          }
        >
          Spin
        </Button>
      </Grid>
    </Grid>
  );
};
