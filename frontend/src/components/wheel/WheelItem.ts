export interface WheelItem {
  index: number;
  name: string;
  x: number;
  y: number;
  x1: number;
  x2: number;
  y1: number;
  y2: number;
}
