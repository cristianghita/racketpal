export interface WheelConfig {
  mass: number;
  tension: number;
  friction: number;
  precision: number;
}
