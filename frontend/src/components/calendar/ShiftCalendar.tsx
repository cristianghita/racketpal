import { useShiftsContext } from "../../hocs/context/shiftsContext";
import "./CustomCalendarStyle.css";
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import "moment/locale/en-gb";
import { useEffect, useState } from "react";
import { OffsetHoursUnix, UnixToDate } from "../../util/dateUtil";
import { useEngineersContext } from "../../hocs/context/engineersContext";
import { createStyles, makeStyles, Theme } from "@material-ui/core";

const localizer = momentLocalizer(moment);

export interface CalendarProps {}

export interface Event {
  title: string;
  start: Date;
  end: Date;
  allDay?: boolean;
  resource?: any;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    calendar: {
      height: "500px",
    },
  })
);

export const ShiftCalendar = (incomingProps: CalendarProps) => {
  const classes = useStyles();
  const { shifts } = useShiftsContext();
  const { engineers } = useEngineersContext();
  const [events, setEvents] = useState<Event[]>([
    { title: "test title", start: new Date(), end: new Date() },
  ]);

  useEffect(() => {
    generateEvents();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [shifts]);

  const generateEvents = (): void => {
    const newEvents: Event[] = [];

    for (let shift of shifts) {
      newEvents.push({
        start: UnixToDate(shift.date),
        end: UnixToDate(OffsetHoursUnix(shift.date, 12)),
        title: engineers.find((e) => e.key === shift.am)?.name ?? "John Doe 💀",
      });

      newEvents.push({
        start: UnixToDate(OffsetHoursUnix(shift.date, 12)),
        end: UnixToDate(OffsetHoursUnix(shift.date, 24)),
        title: engineers.find((e) => e.key === shift.pm)?.name ?? "John Doe 💀",
      });
    }

    console.table({ newEvents });
    setEvents(newEvents);
  };

  return (
    <div>
      <Calendar
        localizer={localizer}
        culture="en-gb"
        events={events}
        startAccessor="start"
        endAccessor="end"
        className={classes.calendar}
        views={["agenda", "month"]}
      />
    </div>
  );
};
