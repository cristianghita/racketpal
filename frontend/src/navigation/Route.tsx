import { RouteComponentProps } from "@reach/router";
import React, { ElementType, FunctionComponent } from "react";

type Props = {
  component: ElementType;
  title: string;
  onRouteChange: (title: string) => void;
} & RouteComponentProps;

const Route: FunctionComponent<Props> = ({
  component: Component,
  title,
  onRouteChange,
  ...rest
}) => {
  onRouteChange(title);
  return <Component {...rest} />;
};

export default Route;
