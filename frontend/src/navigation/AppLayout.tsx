import { createStyles, makeStyles, Theme } from "@material-ui/core";
import withLayout from "../hocs/withLayout";
import AppContent from "./AppContent";
import AppNavBar from "./AppNavBar";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
  })
);

function AppLayout() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppNavBar />
      <AppContent />
    </div>
  );
}

export default withLayout(AppLayout);
