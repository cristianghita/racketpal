import { Router } from "@reach/router";
import React, { useCallback } from "react";
import { LocalizeContextProps } from "react-localize-redux";
import withLayout, { LayoutProps } from "../hocs/withLayout";
import Route from "./Route";
import { ScrollToTop } from "./ScrollToTop";
import { getHomepageURI } from "../util/routes";
import HomePage from "../pages/HomePage";
import { createStyles, makeStyles, Theme, useMediaQuery, useTheme } from "@material-ui/core";
import useLayoutConfig, { LayoutConfig } from "../theme/useLayoutConfig";
import clsx from "clsx";

const useStylesWithLayout = ({ content }: LayoutConfig) => {
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      content: {
        flexGrow: 1,
        padding: theme.spacing(3),
        paddingBottom: theme.spacing(1),
        transition: theme.transitions.create("margin", {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        marginTop: theme.mixins.toolbar.minHeight,
        marginLeft: 0,
      },
      contentSmall: {
        padding: theme.spacing(1),
      },
    })
  );

  return useStyles();
};

const AppContent = ({ setPageTitle, ...props }: {} & LayoutProps & LocalizeContextProps) => {
  const layoutConfig = useLayoutConfig();
  const classes = useStylesWithLayout(layoutConfig);
  const theme = useTheme();
  const isSmallScreen = useMediaQuery(theme.breakpoints.down("sm"));

  const handleRouteChanged = useCallback(
    (title: string) => {
      setPageTitle(title);
    },
    [setPageTitle]
  );

  return (
    <main
      className={clsx(classes.content, {
        [classes.contentSmall]: isSmallScreen,
      })}
    >
      <Router primary={false}>
        <ScrollToTop path="/">
          <Route
            path={`${getHomepageURI()}`}
            component={HomePage}
            title={"Rezerva Vila"}
            onRouteChange={handleRouteChanged}
          />
        </ScrollToTop>
      </Router>
    </main>
  );
};

export default withLayout(AppContent);
