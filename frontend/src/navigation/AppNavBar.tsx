import {
  AppBar,
  createStyles,
  Grid,
  IconButton,
  makeStyles,
  Theme,
  Toolbar,
} from "@material-ui/core";
import clsx from "clsx";
import React from "react";
import withLayout from "../hocs/withLayout";
import useLayoutConfig, { LayoutConfig } from "../theme/useLayoutConfig";
import LogoPNG from "../assets/img/lottery-game.png";

const useStylesWithLayout = ({ navbar }: LayoutConfig) => {
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      appBar: {
        flexGrow: 1,
        transition: theme.transitions.create(["margin", "width"], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        background: "#ebebeb",
      },
      appBarShift: {
        transition: theme.transitions.create(["margin", "width"], {
          easing: theme.transitions.easing.easeOut,
          duration: theme.transitions.duration.enteringScreen,
        }),
      },
      toolbar: theme.mixins.toolbar,
      menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.down("xs")]: {
          marginRight: 0,
        },
      },
      hide: {
        display: "none",
      },
      pageTitle: {
        color: "black",
        fontSize: "20px",
        [theme.breakpoints.down("sm")]: {
          fontSize: "16px",
        },
      },
      navbarLink: {
        color: theme.palette.text.primary,
      },
      menu: {
        marginLeft: theme.spacing(2),
      },
      logo: {
        maxHeight: "3rem",
      },
    })
  );

  return useStyles();
};

const AppNavbar = () => {
  const layoutConfig = useLayoutConfig();
  const classes = useStylesWithLayout(layoutConfig);

  return (
    <div>
      <AppBar
        className={clsx(classes.appBar, {
          [classes.appBarShift]: layoutConfig.navbar.shouldShift,
        })}
        elevation={1}
      >
        <Toolbar className={classes.toolbar}>
          <img src={LogoPNG} alt="logo" className={classes.logo} />
          <IconButton
            color="inherit"
            aria-label="Open drawer"
            edge="end"
            className={classes.pageTitle}
          >
            Support Wheel of Fate
          </IconButton>
          <Grid
            container
            direction="row"
            justify="flex-start"
            spacing={1}
            className={classes.menu}
          ></Grid>
        </Toolbar>
      </AppBar>
      <div className={classes.toolbar} />
    </div>
  );
};

export default withLayout(AppNavbar);
