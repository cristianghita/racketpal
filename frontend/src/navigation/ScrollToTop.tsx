import { RouteComponentProps } from "@reach/router";
import React, { PropsWithChildren } from "react";

export interface ScrollToTopProps extends RouteComponentProps {}

export const ScrollToTop = ({ children, location }: PropsWithChildren<ScrollToTopProps>) => {
  const pathName = location?.pathname;
  React.useEffect(() => window.scrollTo(0, 0), [pathName]);
  return <>{children}</>;
};
