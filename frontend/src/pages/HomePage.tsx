import { FunctionComponent, useEffect, useState } from "react";
import {
  Box,
  Button,
  Container,
  Grid,
  IconButton,
  makeStyles,
  Paper,
  Typography,
} from "@material-ui/core";
import { Wheel } from "../components/wheel/Wheel";
import { Delete, NavigateBefore, NavigateNext } from "@material-ui/icons";
import clsx from "clsx";
import { blue, logoBlue } from "../theme/colors";
import { Engineer } from "../api/engineer/objects/Engineer";
import client from "../api/client";
import { FormatDateShort, OffsetDaysUnix, UnixToDate } from "../util/dateUtil";
import { useSelectedDateContext } from "../hocs/context/selectedDateContext";
import { useEngineersContext } from "../hocs/context/engineersContext";
import { useShiftsContext } from "../hocs/context/shiftsContext";
import { ShiftCalendar } from "../components/calendar/ShiftCalendar";
import useButtonStyles from "../theme/useButtonStyles";

const useStyles = makeStyles((theme) => ({
  paper: {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(5),
  },
  typography: {
    display: "inline-block",
  },
  date: {
    color: logoBlue,
    fontSize: "2rem",
    [theme.breakpoints.down("sm")]: {
      fontSize: "1.7rem",
    },
  },
  shift: {
    color: blue.greyFriends.greyFriend2,
    fontSize: "1.5rem",
    [theme.breakpoints.down("sm")]: {
      fontSize: "1.2rem",
    },
  },
  winner: {
    color: blue.greyFriends.greyFriend1,
  },
  borderShiftOpen: {
    border: `0px solid ${blue.greyFriends.greyFriend1}`,
    padding: "0.6rem 1.2rem 0.6rem 1.2rem",
  },
  borderShiftFilled: {
    border: `1px solid ${blue.greyFriends.greyFriend1}`,
    padding: "0.6rem 1.2rem 0.6rem 1.2rem",
  },
  button: {
    width: "150px",
  },
}));

export const HomePage: FunctionComponent = () => {
  const classes = useStyles();
  const buttons = useButtonStyles();
  const { selectedDate, setSelectedDate } = useSelectedDateContext();

  const [am, setAm] = useState<Engineer | undefined>();
  const [pm, setPm] = useState<Engineer | undefined>();
  const { engineers } = useEngineersContext();
  const { shifts, refreshShifts } = useShiftsContext();
  const [isLoading, setIsLoading] = useState(false);

  const showShiftEng = () => {
    const currentShift = shifts.find((s) => s.date === selectedDate);
    if (currentShift && engineers.length !== 0) {
      setAm(engineers.find((e) => e.key === currentShift.am));
      setPm(engineers.find((e) => e.key === currentShift.pm));
    } else {
      setAm(undefined);
      setPm(undefined);
    }
  };

  useEffect(() => {
    showShiftEng();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [engineers, shifts, selectedDate]);

  const handleDateChange = (days: number) => {
    setSelectedDate(OffsetDaysUnix(selectedDate!, days));
    showShiftEng();
  };

  const handleDeleteShifts = async () => {
    try {
      setIsLoading(true);
      await client.shift.deleteShifts();
      await refreshShifts();
    } catch (e) {
      // log error
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Container maxWidth="md">
      <Paper elevation={3} className={classes.paper}>
        <Grid container direction="column" justify="center" alignItems="center" spacing={1}>
          <Grid item xs={12}>
            <Grid container direction="column" justify="center" alignItems="center" spacing={1}>
              <Grid item>
                <Grid container direction="row" alignItems="center" justify="center" spacing={1}>
                  <Grid item>
                    <IconButton
                      color="primary"
                      onClick={() => handleDateChange(-1)}
                      disabled={isLoading}
                    >
                      <NavigateBefore fontSize="large" />
                    </IconButton>
                  </Grid>
                  <Grid item>
                    <Typography className={clsx(classes.typography, classes.date)}>
                      {selectedDate ? FormatDateShort(UnixToDate(selectedDate)) : null}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <IconButton
                      color="primary"
                      onClick={() => handleDateChange(1)}
                      disabled={isLoading}
                    >
                      <NavigateNext fontSize="large" />
                    </IconButton>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid container direction="row" alignItems="center" justify="center" spacing={1}>
                  <Grid item>
                    <Typography className={clsx(classes.shift, classes.typography)}>
                      AM:{" "}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Box
                      className={am ? classes.borderShiftFilled : classes.borderShiftOpen}
                      borderRadius="borderRadius"
                    >
                      <Typography
                        className={
                          am
                            ? clsx(classes.shift, classes.typography, classes.winner)
                            : clsx(classes.shift, classes.typography)
                        }
                      >
                        {am?.name ?? "TBD"}
                      </Typography>
                    </Box>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid container direction="row" alignItems="center" justify="center" spacing={1}>
                  <Grid item>
                    <Typography className={clsx(classes.shift, classes.typography)}>
                      PM:{" "}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Box
                      className={pm ? classes.borderShiftFilled : classes.borderShiftOpen}
                      borderRadius="borderRadius"
                    >
                      <Typography
                        className={
                          pm
                            ? clsx(classes.shift, classes.typography, classes.winner)
                            : clsx(classes.shift, classes.typography)
                        }
                      >
                        {pm?.name ?? "TBD"}
                      </Typography>
                    </Box>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            {(!am || !pm) && selectedDate ? (
              <Wheel
                setAm={setAm}
                setPm={setPm}
                isLoading={isLoading}
                setIsLoading={setIsLoading}
              />
            ) : null}
          </Grid>
          <Grid item>{am && pm ? <ShiftCalendar /> : null}</Grid>
          <Grid item>
            <Button
              className={clsx(buttons.classicBlue, classes.button)}
              startIcon={<Delete />}
              disabled={isLoading || shifts.length === 0}
              onClick={() => handleDeleteShifts()}
            >
              Delete shifts
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </Container>
  );
};

export default HomePage;
