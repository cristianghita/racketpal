import { applyMiddleware, combineReducers, createStore } from "redux";
import { layoutReducer } from "./layout/reducers";
import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

const rootReducer = combineReducers({
  layout: layoutReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

export default function configureStore() {
  const middlewares = [thunkMiddleware];
  const middlewareEnhancer = applyMiddleware(...middlewares);
  const composeEnhancers = composeWithDevTools({
    trace: true,
  });

  const store = createStore(rootReducer, composeEnhancers(middlewareEnhancer));

  return store;
}
