import { LayoutActionType, UPDATE_PAGE_TITLE } from "./types";

export function updatePageTitle(value: string): LayoutActionType {
  return {
    type: UPDATE_PAGE_TITLE,
    value,
  };
}
