export const UPDATE_PAGE_TITLE = "UPDATE_PAGE_TITLE";

export interface LayoutState /* extends DrawerState */ {
  pageTitle: string;
}

interface UpdatePageTitleAction {
  type: typeof UPDATE_PAGE_TITLE;
  value: string;
}

export type LayoutActionType = UpdatePageTitleAction;
