import { LayoutActionType, LayoutState, UPDATE_PAGE_TITLE } from "./types";

const initialState: LayoutState = {
  pageTitle: "Rezerva-vila initial Title",
};

export function layoutReducer(state = initialState, action: LayoutActionType): LayoutState {
  switch (action.type) {
    case UPDATE_PAGE_TITLE:
      return {
        ...state,
        pageTitle: action.value,
      };
    default:
      return state;
  }
}
