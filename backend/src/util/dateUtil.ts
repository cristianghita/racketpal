export const DateToUnix = (date: Date): number => {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() / 1000;
};

export const UnixToDate = (date: number): Date => {
  return new Date(date * 1000);
};

export const UnixToDateOffs = (date: number, offsetHours: number): Date => {
  return new Date((date + offsetHours * 3600) * 1000);
};

export const DateToUnixOffsetDays = (date: Date, offsetDays: number): number => {
  const clonedDate = new Date(date);
  clonedDate.setDate(clonedDate.getDate() + offsetDays);
  return DateToUnix(clonedDate);
};

export const FormatDateShort = (date: Date) => {
  return (
    date.getDate().toString().padStart(2, "0") +
    "-" +
    (date.getMonth() + 1).toString().padStart(2, "0") +
    "-" +
    date.getFullYear().toString()
  );
};

export const GetCurrentDateWithoutTime = (): Date => {
  return new Date(new Date().toDateString());
};

export const OffsetDaysUnix = (date: number, offsetDays: number): number => {
  return date + offsetDays * 86400; // 3600 - 1 hour, 86400 - 24 hours
};
