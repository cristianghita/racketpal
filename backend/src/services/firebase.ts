import { config } from "../config/global";
import firebase from "firebase";

const firebaseDatabase = () => {
  if (firebase.apps.length === 0) {
    firebase.initializeApp(config.firebaseConfig);
  }

  return firebase.database();
};

export default {
  database: firebaseDatabase,
};
