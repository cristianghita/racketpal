import Router from "@koa/router";
import general from "../components/general/controller";
// import user from "../components/user/controller";
import engineer from "../components/engineer/controller";
import shift from "../components/shift/controller";

const unprotectedRouter = new Router();

const engineerApi = "engineer";
const shiftApi = "shift";

unprotectedRouter.get("/", general.helloWorld);
unprotectedRouter.get(`/${engineerApi}/get-engineers`, engineer.getEngineers);
unprotectedRouter.post(`/${engineerApi}/create-engineer`, engineer.createEngineer);
unprotectedRouter.get(`/${engineerApi}/get-winners/:date`, engineer.getWinners);
unprotectedRouter.get(`/${shiftApi}/get-shifts/:date`, shift.getShifts);
unprotectedRouter.get(`/${shiftApi}/delete-shifts`, shift.deleteShifts);

export { unprotectedRouter };
