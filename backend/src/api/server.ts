import Koa from "koa";
import bodyParser from "koa-bodyparser";
import json from "koa-json";
import helmet from "koa-helmet";
import cors from "@koa/cors";

import { config } from "../config/global";
import { unprotectedRouter } from "./routes/unprotectedRoutes";

const app = new Koa();

app.use(helmet());
app.use(cors());
app.use(json());
app.use(bodyParser());

app.use(unprotectedRouter.routes()).use(unprotectedRouter.allowedMethods());

app.listen(process.env.port || process.env.PORT || config.port);
console.log(`Server running on port [${config.port}]`);
