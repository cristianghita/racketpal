import { Context } from "node:vm";
import firebase from "../../../services/firebase";
import { Engineer } from "./objects/Engineer";
import { StatusCodes as status } from "http-status-codes";
import shift from "../shift/controller";
import engineerData from "./data";
import shiftBusiness from "../shift/business";

const ref = firebase.database().ref("engineer");

export default class EngineerController {
  //#region CREATE
  public static async createEngineer(ctx: Context): Promise<void> {
    try {
      const name: string = ctx.request.body.name;
      if (!name) {
        ctx.status = status.BAD_REQUEST;
        ctx.body = { isError: true };
      } else {
        await ref.push({ name });
        ctx.status = status.CREATED;
        ctx.body = { isError: false };
      }
    } catch (e) {
      ctx.status = status.INTERNAL_SERVER_ERROR;
      ctx.body = { isError: true, e };
    }
  }
  //#endregion

  //#region READ

  public static async getEngineers(ctx: Context): Promise<void> {
    try {
      const engineers: Engineer[] = await engineerData.getEngineers();

      ctx.status = status.OK;
      ctx.body = { isError: false, response: { engineers } };
    } catch (e) {
      ctx.status = status.INTERNAL_SERVER_ERROR;
      ctx.body = { isError: true, e };
    }
  }

  public static async getWinners(ctx: Context): Promise<void> {
    try {
      const date: number = parseInt(ctx.params.date); // todo: add validation
      const winners = await shiftBusiness.getShiftWinners(date);

      await shift.saveShift(date, winners[0].key!, winners[1].key!);

      ctx.status = status.OK;
      ctx.body = { isError: false, response: { winners } };
    } catch (e) {
      ctx.status = status.INTERNAL_SERVER_ERROR;
      ctx.body = { isError: true, e };
    }
  }

  //#endregion

  //#region UPDATE
  //#endregion

  //#region DELETE
  //#endregion
}
