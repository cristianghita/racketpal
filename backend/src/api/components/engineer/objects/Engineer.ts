import { MinLength } from "class-validator";

export class Engineer {
  @MinLength(2)
  name: string;

  @MinLength(10)
  key?: string;
}
