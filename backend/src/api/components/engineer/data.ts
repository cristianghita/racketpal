import firebase from "../../../services/firebase";
import { Engineer } from "./objects/Engineer";

const ref = firebase.database().ref("engineer");

export default class EngineerData {
  public static async getEngineers(): Promise<Engineer[]> {
    const engineers: Engineer[] = [];
    const snapshot = await ref.once("value");
    const dbEngineers: { [key: string]: Engineer } = snapshot.val();

    for (const [key, engineer] of Object.entries(dbEngineers)) {
      engineers.push({ key, name: engineer.name });
    }

    return engineers;
  }
}
