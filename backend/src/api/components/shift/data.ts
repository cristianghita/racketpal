import { DateToUnixOffsetDays, UnixToDate } from "../../../util/dateUtil";
import { Shift } from "./objects/Shift";
import firebase from "../../../services/firebase";

const ref = firebase.database().ref("shift");

export default class ShiftData {
  public static async getShifts(dateUnix: number): Promise<Shift[]> {
    const shifts: Shift[] = [];
    const date = UnixToDate(dateUnix);
    const minDateUnix = DateToUnixOffsetDays(date, -13);
    const maxDateUnix = DateToUnixOffsetDays(date, 15);

    const snapshot = await ref
      .orderByKey()
      .startAt(minDateUnix.toString())
      .endAt(maxDateUnix.toString()) // asdasda
      .once("value");
    const dbShifts: { [date: number]: Shift } = snapshot.val();

    if (dbShifts) {
      // if nothing found in that interval it returns null
      for (const [date, dbShift] of Object.entries(dbShifts)) {
        shifts.push({ date: parseInt(date), am: dbShift.am, pm: dbShift.pm });
      }
    }

    return shifts;
  }

  public static async getShiftsOffsDays(dateUnix: number, days: number): Promise<Shift[]> {
    const shifts: Shift[] = [];
    const date = UnixToDate(dateUnix);
    let minDateUnix: number, maxDateUnix: number;
    if (days < 0) {
      minDateUnix = DateToUnixOffsetDays(date, days + 1);
      maxDateUnix = DateToUnixOffsetDays(date, 0);
    } else {
      minDateUnix = DateToUnixOffsetDays(date, 0);
      maxDateUnix = DateToUnixOffsetDays(date, days + 1);
    }

    const snapshot = await ref
      .orderByKey()
      .startAt(minDateUnix.toString())
      .endAt(maxDateUnix.toString()) // asdasda
      .once("value");
    const dbShifts: { [date: number]: Shift } = snapshot.val();

    if (dbShifts) {
      // if nothing found in that interval it returns null
      for (const [date, dbShift] of Object.entries(dbShifts)) {
        shifts.push({ date: parseInt(date), am: dbShift.am, pm: dbShift.pm });
      }
    }

    return shifts;
  }
}
