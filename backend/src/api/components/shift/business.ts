import { DateToUnixOffsetDays, OffsetDaysUnix, UnixToDate } from "../../../util/dateUtil";
import { Shift } from "./objects/Shift";
import firebase from "../../../services/firebase";
import engineerData from "../engineer/data";
import shiftData from "./data";
import { Engineer } from "../engineer/objects/Engineer";

export interface EngineerShiftCount {
  [engineerKey: string]: number;
}

const preferredShifts = 2;

export default class ShiftBusiness {
  public static async getShiftWinners(dateUnix: number): Promise<Engineer[]> {
    const engineers = await engineerData.getEngineers();
    const shifts = await shiftData.getShiftsOffsDays(dateUnix, -14); // last 2 weeks

    const available = this.removePrevShiftEngineers(dateUnix, shifts, engineers);
    const preferred = this.filterPreferredEngineers(shifts, available);

    if (preferred.length === 0) {
      return this.getTwoRandomEngineers(available);
    } else if (preferred.length === 1) {
      const winners: Engineer[] = [];
      winners.push(preferred[0]); // add preferred
      available.splice(available.indexOf(available.find((e) => e.key === preferred[0].key)!), 1);
      winners.push(available[Math.floor(Math.random() * available.length)]); // add a random available that is not preferred
      return winners;
    } else {
      return this.getTwoRandomEngineers(preferred);
    }
  }

  static getTwoRandomEngineers(engineers: Engineer[]) {
    const random: Engineer[] = [];
    for (let i = 0; i < 2; i++) {
      const ranIndex = Math.floor(Math.random() * engineers.length);
      random.push(engineers[ranIndex]);
      engineers.splice(ranIndex, 1);
    }

    return random;
  }

  static removePrevShiftEngineers(dateUnix: number, shifts: Shift[], engineers: Engineer[]) {
    const available = engineers.slice();
    if (shifts.length === 0) {
      return available;
    }
    const offset = OffsetDaysUnix(dateUnix, -1);
    const prevShift = shifts.find((s) => s.date === OffsetDaysUnix(dateUnix, -1));
    if (prevShift) {
      // last shift engineers are unavailable
      available.splice(available.indexOf(available.find((e) => e.key === prevShift.am)!), 1);
      available.splice(available.indexOf(available.find((e) => e.key === prevShift.pm)!), 1);
    }

    return available;
  }

  static filterPreferredEngineers(shifts: Shift[], available: Engineer[]) {
    const preferred = available.slice();
    const shiftCounts: EngineerShiftCount = {};

    for (let shift of shifts) {
      available.forEach((e) => {
        if (shift.am === e.key || shift.pm === e.key) {
          shiftCounts[e.key] = e.key in shiftCounts ? shiftCounts[e.key] + 1 : 1;
        }
      });
    }

    for (let [key, count] of Object.entries(shiftCounts)) {
      if (count >= preferredShifts) {
        preferred.splice(preferred.indexOf(preferred.find((e) => e.key === key)!), 1);
      }
    }

    return preferred;
  }
}
