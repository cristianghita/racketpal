import { Context } from "node:vm";
import firebase from "../../../services/firebase";
import { DateToUnix, DateToUnixOffsetDays, UnixToDate } from "../../../util/dateUtil";
import { StatusCodes as status } from "http-status-codes";
import { Shift } from "./objects/Shift";
import shiftData from "./data";

const ref = firebase.database().ref("shift");

export default class ShiftController {
  public static async saveShift(date: number, amKey: string, pmKey: string): Promise<void> {
    await ref.child(date.toString()).set({
      am: amKey,
      pm: pmKey,
      date,
    });
  }

  public static async getShifts(ctx: Context): Promise<void> {
    try {
      const dateUnix: number = parseInt(ctx.params.date, 10);
      // todo: add validation

      const shifts: Shift[] = await shiftData.getShifts(dateUnix);

      ctx.status = status.OK;
      ctx.body = { isError: false, response: { shifts } };
    } catch (e) {
      ctx.status = status.INTERNAL_SERVER_ERROR;
      ctx.body = { isError: true, e };
    }
  }

  public static async deleteShifts(ctx: Context): Promise<void> {
    try {
      await ref.remove();
      ctx.status = status.OK;
      ctx.body = { isError: false };
    } catch (e) {
      ctx.status = status.INTERNAL_SERVER_ERROR;
      ctx.body = { isErroro: true };
    }
  }
}
