import { BaseContext } from "koa";
import { StatusCodes as status } from "http-status-codes";

export default class GeneralController {
  public static async helloWorld(ctx: BaseContext): Promise<void> {
    ctx.body = "Hello World";
    ctx.status = status.OK;
  }
}
