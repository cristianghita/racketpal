import { BaseContext } from "koa";
import { StatusCodes as status } from "http-status-codes";
import firebase from "../../../services/firebase";

const db = firebase.database();

export default class UserController {
  public static async getUsers(ctx: BaseContext): Promise<void> {
    try {
      const snapshot = await db.ref("user").once("value");
      ctx.body = { users: snapshot.val() };
      ctx.status = status.OK;
    } catch (e) {
      ctx.body = { errorMessage: e.message };
      ctx.status = status.INTERNAL_SERVER_ERROR;
    }
  }
}
